extends CharacterBody2D


@export var movement_speed = 80.0
@export var hp = 1

@onready var player = get_tree().get_first_node_in_group("player")
@onready var sprite: Sprite2D = $Sprite2D
@onready var anim: AnimationPlayer = $AnimationPlayer

func _ready():
	anim.play("walk")

func _physics_process(delta):
	var direction = global_position.direction_to(player.global_position)
	velocity = direction*movement_speed
	move_and_slide()
	
	if direction.x > 0.1:
		sprite.flip_h = true
	else:
		sprite.flip_h = false
		
	
		


func _on_hurt_box_hurt(damage):
	hp -= damage
	print("HP reamining life: " + str(hp))
	if hp <= 0:
		queue_free()
